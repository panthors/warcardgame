package org.patrickjoiner.games.war.test;

import org.junit.Test;
import org.patrickjoiner.games.war.Card;
import org.patrickjoiner.games.war.Game;
import org.patrickjoiner.games.war.Player;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
/**
 * Tests for Player
 */
public class PlayerTests {

    /**
     * Test adding a card.
     *
     * Result:  Unplayed cards should increase by one each time.
     */
    @Test
    public void testPlayerAddCard() {
        Player player = new Player(1);
        player.addCard(new Card(Card.Suit.SPADE, Card.Rank.JACK));
        assertEquals(1, player.numberOfUnplayedCards());
        player.addCard(new Card(Card.Suit.SPADE, Card.Rank.ACE));
        assertEquals(2,player.numberOfUnplayedCards());
    }

    /**
     * Test playing a card.
     * Result:  Unplayed cards should decrease by 1, played cards increase by 1.
     */
    @Test
    public void testPlayerPlayCard() {
        Player player = new Player(1);
        player.addCard(new Card(Card.Suit.SPADE, Card.Rank.JACK));
        assertEquals(1, player.numberOfUnplayedCards());
        assertEquals(0, player.numberOfPlayedCards());

        player.playCard(true,true);
        assertEquals(0,player.numberOfUnplayedCards());
        assertEquals(1,player.numberOfPlayedCards());
    }
    /**
     * Test playing a card with no cards
     * Result:  Unplayed and played cards should be 0.
     */
    @Test
    public void testPlayerPlayCardWithNoCards() {
        Player player = new Player(1);
        player.playCard(true,true);
        assertEquals(0,player.numberOfUnplayedCards());
        assertEquals(0,player.numberOfPlayedCards());
    }

    /**
     * Test first card value - first card is jack of spades
     *
     * Result:  Value should be 11 (jack)
     */
    @Test
    public void testPlayerFirstCardValueWithFirstCard() {
        Player player = new Player(1);
        player.addCard(new Card(Card.Suit.SPADE, Card.Rank.JACK));
        // play card to move it to played cards
        player.playCard(true,true);
        assertEquals(11, player.getFirstPlayedCardValue());
    }
    /**
     * Test first card value - no first card
     *
     * Result:  Value should be 0.
     */
    @Test
    public void testPlayerFirstCardValueWithNoFirstCard() {
        Player player = new Player(1);
        player.addCard(new Card(Card.Suit.SPADE, Card.Rank.JACK));
        assertEquals(0, player.getFirstPlayedCardValue());
    }
    /**
     * Test removing a card when there are cards.
     *
     * Result:  Unplayed cards should decrease by 1 (to 0). The removed card
     *     should be the same as the one added.
     */
    @Test
    public void testPlayerRemoveCard() {
        Player player = new Player(1);

        Card card = new Card(Card.Suit.SPADE, Card.Rank.JACK);
        player.addCard(card);
        assertEquals(1, player.numberOfUnplayedCards());

        Card removedCard = player.removeCard();
        assertEquals(0,player.numberOfUnplayedCards());

        assertSame(card,removedCard);
    }
    /**
     * Test removing a card when there are cards.
     *
     * Result:  Unplayed cards should decrease by 1 (to 0). The removed card
     *     should be the same as the one added.
     */
    @Test
    public void testPlayerRemoveCardWithNoCards() {
        Player player = new Player(1);

        Card removedCard = player.removeCard();
        assertEquals(0,player.numberOfUnplayedCards());
        assertNull(removedCard);
    }

    /**
     * Test removing a card when there are cards.
     *
     * Result:  Unplayed cards should decrease by 1 (to 0). The removed card
     *     should be the same as the one added.
     */
    @Test
    public void testPlayerRemoveFirstPlayedCard() {
        Player player = new Player(1);

        Card card = new Card(Card.Suit.SPADE, Card.Rank.JACK);
        player.addCard(card);
        player.playCard(true);

        Card removedCard = player.removeFirstPlayedCard();
        assertEquals(0,player.numberOfPlayedCards());

        assertSame(card,removedCard);
    }
    /**
     * Test removing a card when there are cards.
     *
     * Result:  Unplayed cards should decrease by 1 (to 0). The removed card
     *     should be the same as the one added.
     */
    @Test
    public void testPlayerRemoveFirstPlayedCardWithNoPlayedCards() {
        Player player = new Player(1);
        Card removedCard = player.removeFirstPlayedCard();
        assertEquals(0,player.numberOfPlayedCards());
        assertNull(removedCard);
    }
}

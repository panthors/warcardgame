package org.patrickjoiner.games.war.test;

import org.junit.Before;
import org.junit.Test;
import org.patrickjoiner.games.war.*;
import org.patrickjoiner.games.war.round.NormalRound;
import org.patrickjoiner.games.war.round.Round;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
/**
 * Tests for NormalRound
 */
public class NormalRoundTests {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private List<Player> twoPlayers;
    private List<Card> discardPile;

    @Before
    public void testSetup() {
        twoPlayers = new ArrayList<>();
        twoPlayers.add(new Player(1));
        twoPlayers.add(new Player(2));

        this.discardPile = new ArrayList<>();
    }


    /**
     * Test round number after creation
     * Result:  Round number should be the same as what was passed in.
     */
    @Test
    public void testNormalRoundSetupNumber() {
        NormalRound round = new NormalRound(2, twoPlayers, new ArrayList<>());
        assertEquals(2,round.getRoundNumber());
    }

    /**
     * Test player count after creation
     * Result:  Player count should be the same as the size of the array passed in.
     */
    @Test
    public void testNormalRoundSetupNumberOfPlayers() {
        NormalRound round = new NormalRound(1, twoPlayers, discardPile);
        assertEquals(2,round.numberOfPlayers());
    }

    /**
     * Test discard pile after creation
     *
     * Result:  Discard pile should have same cards as passed in (0)
     */
    @Test
    public void testNormalRoundSetupEmptyDiscardPile() {
        NormalRound round = new NormalRound(1, twoPlayers, discardPile);
        assertEquals(0,round.getDiscardPile().size());
    }

    /**
     * Test discard pile after creation
     *
     * Result:  Discard pile should have same cards as passed in (0)
     */
    @Test
    public void testNormalRoundSetupDiscardPileWithOneCard() {
        discardPile.add(new Card(Card.Suit.CLUB, Card.Rank.ACE));
        NormalRound round = new NormalRound(1, twoPlayers, discardPile);
        assertEquals(1,round.getDiscardPile().size());
    }

    /**
     * Test passing null players.
     *
     * Result:  There should be zero players in the round.
     */
    @Test
    public void testNormalRoundNullPlayersSetup() {
        NormalRound round = new NormalRound(1,null, discardPile);
        assertEquals(0,round.numberOfPlayers());
    }

    /**
     * Test passing null discardPile
     *
     * Result:  discard pile should be created with 0 cards.
     */
    @Test
    public void testNormalRoundNullDiscardPileSetup() {
        NormalRound round = new NormalRound(1,twoPlayers, null);
        assertNotNull(round.getDiscardPile());
        assertEquals(0,round.getDiscardPile().size());
    }

    /**
     * Test playing cards in a normal round - player 1 wins.
     *
     * Result: Player 1 should have 2 cards, Player 2 should have 0 cards,
     *     and the discard pile should have 0 cards.
     */
    @Test
    public void testNormalRoundWithWinner() {

        Player player1 = twoPlayers.get(0);
        Player player2 = twoPlayers.get(1);

        Card card1 = new Card(Card.Suit.CLUB, Card.Rank.ACE);
        Card card2 = new Card(Card.Suit.HEART, Card.Rank.TWO);

        player1.addCard(card1);
        player2.addCard(card2);

        Round round = new NormalRound(1,twoPlayers,discardPile);
        round.play();

        // player 1 will win, so will have 2 cards.
        assertEquals(2,player1.numberOfUnplayedCards());

        // player 2 will have 0 cards
        assertEquals(0,player2.numberOfUnplayedCards());

        // discard pile will have 0 cards
        assertEquals(0,round.getDiscardPile().size());

    }

    /**
     * Test playing cards in a normal round with a tie
     *
     * Result: All cards (2) should go to the discard pile and players
     *     should have 0 cards.
     */
    @Test
    public void testNormalRoundWithTie() {

        Card card1 = new Card(Card.Suit.CLUB, Card.Rank.ACE);
        Card card2 = new Card(Card.Suit.DIAMOND,Card.Rank.ACE);

        Player player1 = twoPlayers.get(0);
        Player player2 = twoPlayers.get(1);

        player1.addCard(card1);
        player2.addCard(card2);

        Round round = new NormalRound(1, twoPlayers, discardPile);
        round.play();

        logger.info("Player 1: {},{}", player1.numberOfUnplayedCards(),player1.numberOfPlayedCards());
        logger.info("Player 2: {},{}", player2.numberOfUnplayedCards(),player2.numberOfPlayedCards());


        // there should be 2 players remaining, indicating a tie
        assertEquals(round.numberOfPlayers(),2);

        // player 1 will have 0 cards (all in pile)
        assertEquals(0,player1.numberOfUnplayedCards());

        // player 2 will have 0 cards (all in pile)
        assertEquals(0,player2.numberOfUnplayedCards());

        // discard pile will have 2 cards
        assertEquals(2, round.getDiscardPile().size());

    }

}


package org.patrickjoiner.games.war.test;

import org.junit.Rule;
import org.junit.contrib.java.lang.system.SystemErrRule;
import org.patrickjoiner.games.war.Game;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
/**
 * Tests for Game
 */
public class GameTests {
    @Rule
    public final SystemErrRule systemErrRule = new SystemErrRule().enableLog();

    /**
     * Initialize game with 0 players
     * Result:  syserr should get an "aborting game" message and isReady() should be false.
     */
    @Test
    public void testGameSetupWithZeroPlayers() {
        Game game = new Game(0);
        assertFalse(game.isReady());
        assertThat(systemErrRule.getLog(),containsString("aborting game"));
    }

    /**
     * Initialize game with 1 player.
     * Result:  syserr should get an "aborting game" message and isReady() should be false.
     */
    @Test
    public void testGameSetupWithOnePlayer() {
        Game game = new Game(1);
        assertFalse(game.isReady());
        assertThat(systemErrRule.getLog(),containsString("aborting game"));
    }

    /**
     * Initialize game with 2 players.
     *
     * Result:  isReady() should be true.
     */
    @Test
    public void testGameSetupWithTwoPlayers() {
        Game game = new Game(2);
        assertTrue(game.isReady());
    }

    /**
     * Initialize game with 3 players.
     * Result: syserr should get an "aborting game" message and isReady() should be false.
     */
    @Test
    public void testGameSetupWithThreePlayers() {
        Game game = new Game(3);
        assertFalse(game.isReady());
        assertThat(systemErrRule.getLog(),containsString("aborting game"));
    }

    /**
     * Initialize game with 53 players.
     * Result:  syserr should get an "aborting game" message and isReady() should be false.
     */
    @Test
    public void testGameSetupWithFiftyThreePlayers() {
        Game game = new Game(53);
        assertFalse(game.isReady());
        assertThat(systemErrRule.getLog(),containsString("aborting game"));
    }

    /**
     * Game with -1 players
     * Result:  syserr should get an "aborting game" message and isReady() should be false.
     */
    @Test
    public void testGameSetupWithNegativeOnePlayers() {
        Game game = new Game(-1);
        assertFalse(game.isReady());
        assertThat(systemErrRule.getLog(),containsString("aborting game"));

    }


}

package org.patrickjoiner.games.war.test;

import org.junit.Test;
import org.patrickjoiner.games.war.Card;
import org.patrickjoiner.games.war.Deck;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
/**
 * Tests for Card
 */
public class CardTests {
    /**
     * Create a card with no values.
     *
     * Result:  Card should default to ace of spaces
     */
    @Test
    public void testCardDefault() {
        Card card = new Card();
        assertEquals(Card.Suit.SPADE, card.getSuit());
        assertEquals(Card.Rank.ACE, card.getRank());
    }

    /**
     * Create a card with null constructor values
     * Result:  Card should default to ace of spaces
     */
    @Test
    public void testCardWithNullConstructorValues() {
        Card card = new Card(null,null);

        assertEquals(Card.Suit.SPADE, card.getSuit());
        assertEquals(Card.Rank.ACE, card.getRank());
    }

    /**
     * Create a card with constructor values
     *
     * Result:  Card should be the same as the constructors passed in
     */
    @Test
    public void testCardConstructor() {
        for (Card.Suit suit: Card.Suit.values()) {
            for (Card.Rank rank: Card.Rank.values()) {
                Card card = new Card(suit, rank);
                assertNotNull(card);
                assertEquals(suit, card.getSuit());
                assertEquals(rank, card.getRank());
            }
        }
    }

    /**
     * Check the output of the Ascii card representation (7 of diamonds)
     *
     * Result:  Ascii representation should match the created card.
     */
    @Test
    public void testCardAsciiFaceSevenOfDiamondsShowing() {
        String cardString =
                        " ----- \n" +
                        "|7    |\n" +
                        "|  ♦  |\n" +
                        "|    7|\n" +
                        " ----- ";

        Card card = new Card(Card.Suit.DIAMOND,Card.Rank.SEVEN);
        assertEquals(cardString, card.getAsciiImage(true));
    }
    /**
     * Check the output of the Ascii card representation (ace of spades)
     *
     * Result:  Ascii representation should match the created card.
     */
    @Test
    public void testCardAsciiFaceAceOfSpadesShowing() {
        String cardString =
                        " ----- \n" +
                        "|A    |\n" +
                        "|  ♠  |\n" +
                        "|    A|\n" +
                        " ----- ";

        Card card = new Card(Card.Suit.SPADE,Card.Rank.ACE);
        assertEquals(cardString, card.getAsciiImage(true));
    }
    /**
     * Check the output of the Ascii card representation (queen of hearts)
     *
     * Result:  Ascii representation should match the created card.
     */
    @Test
    public void testCardAsciiFaceQueenOfHeartsShowing() {
        String cardString =
                        " ----- \n" +
                        "|Q    |\n" +
                        "|  ♥  |\n" +
                        "|    Q|\n" +
                        " ----- ";

        Card card = new Card(Card.Suit.HEART,Card.Rank.QUEEN);
        assertEquals(cardString, card.getAsciiImage(true));
    }
    /**
     * Check the output of the Ascii card representation (10 of clubs)
     *
     * Result:  Ascii representation should match the created card.
     */
    @Test
    public void testCardAsciiFaceTenOfClubsShowing() {
        String cardString =
                        " ----- \n" +
                        "|10   |\n" +
                        "|  ♣  |\n" +
                        "|   10|\n" +
                        " ----- ";

        Card card = new Card(Card.Suit.CLUB,Card.Rank.TEN);
        assertEquals(cardString, card.getAsciiImage(true));
    }
    /**
     * Check the output of the Ascii card representation for a non-showing card.
     * Result:  Ascii representation should be the back of a card.
     */
    @Test
    public void testCardAsciiFaceNotShowing() {
        String cardFaceString =
                " ----- \n" +
                "|*****|\n" +
                "|*****|\n" +
                "|*****|\n" +
                " ----- ";
        Card card = new Card(Card.Suit.DIAMOND,Card.Rank.JACK);
        assertEquals(cardFaceString,card.getAsciiImage(false));
    }

}

package org.patrickjoiner.games.war.test;

import org.junit.Before;
import org.junit.Test;
import org.patrickjoiner.games.war.Card;
import org.patrickjoiner.games.war.Player;
import org.patrickjoiner.games.war.PlayerFirstCardComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
/**
 * Tests for PlayerFirstCardComparator
 */
public class PlayerFirstCardComparatorTests {
    List<Player> players;


    /**
     * Set up 2 players for each test.
     */
    @Before
    public void setupPlayers() {
        players = new ArrayList<>();

        Player player1 = new Player(1);
        Player player2 = new Player(1);

        players.add(player1);
        players.add(player2);
    }

    /**
     * Compare first card of two players with the same suit.
     *
     * Result: The first player should win.
     */
    @Test
    public void testCompareFirstCardTwoPlayersSameSuitPlayerOneWins() {

        Player player1 = players.get(0);
        Player player2 = players.get(1);

        Card card1 = new Card(Card.Suit.SPADE, Card.Rank.JACK);
        Card card2 = new Card(Card.Suit.SPADE, Card.Rank.TWO);

        player1.addCard(card1);
        player2.addCard(card2);

        // Play card to move it to played cards
        player1.playCard(true);
        player2.playCard(true);

        Player maxPlayer = Collections.max(players,new PlayerFirstCardComparator());

        assertEquals(player1.getFirstPlayedCardValue(),maxPlayer.getFirstPlayedCardValue());
    }

    /**
     * Compare first card of two players with the different suits
     *    This is just to prove that the suit doesn't matter.
     *
     * Result: The first player should win.
     */
    @Test
    public void testCompareFirstCardTwoPlayersDifferentSuitPlayerOneWins() {

        Player player1 = players.get(0);
        Player player2 = players.get(1);

        Card card1 = new Card(Card.Suit.DIAMOND, Card.Rank.JACK);
        Card card2 = new Card(Card.Suit.SPADE, Card.Rank.TWO);

        player1.addCard(card1);
        player2.addCard(card2);

        // Play card to move it to played cards
        player1.playCard(true);
        player2.playCard(true);

        Player maxPlayer = Collections.max(players,new PlayerFirstCardComparator());

        assertEquals(player1.getFirstPlayedCardValue(),maxPlayer.getFirstPlayedCardValue());
    }


    /**
     * Compare first card of two players with the same suit.
     *
     * Result: The second player should win.
     */
    @Test
    public void testCompareFirstCardTwoPlayersSameSuitPlayerTwoWins() {

        Player player1 = players.get(0);
        Player player2 = players.get(1);

        Card card1 = new Card(Card.Suit.SPADE, Card.Rank.JACK);
        Card card2 = new Card(Card.Suit.SPADE, Card.Rank.ACE);

        player1.addCard(card1);
        player2.addCard(card2);

        // Play card to move it to played cards
        player1.playCard(true);
        player2.playCard(true);

        Player maxPlayer = Collections.max(players,new PlayerFirstCardComparator());

        assertEquals(player2.getFirstPlayedCardValue(),maxPlayer.getFirstPlayedCardValue());
    }

    /**
     * Compare first card of two players with the different suits
     *    This is just to prove that the suit doesn't matter.
     *
     * Result: The second player should win.
     */
    @Test
    public void testCompareFirstCardTwoPlayersDifferentSuitPlayerTwoWins() {

        Player player1 = players.get(0);
        Player player2 = players.get(1);

        Card card1 = new Card(Card.Suit.DIAMOND, Card.Rank.JACK);
        Card card2 = new Card(Card.Suit.SPADE, Card.Rank.ACE);

        player1.addCard(card1);
        player2.addCard(card2);

        // Play card to move it to played cards
        player1.playCard(true);
        player2.playCard(true);

        Player maxPlayer = Collections.max(players,new PlayerFirstCardComparator());

        assertEquals(player2.getFirstPlayedCardValue(),maxPlayer.getFirstPlayedCardValue());
    }


    /**
     * Compare first card of two players with the same suit
     *     and same rank.
     *
     * Result: The max player card value will be the same as both player's value
     */
    @Test
    public void testCompareFirstCardTwoPlayersDifferentSuitTie() {

        Player player1 = players.get(0);
        Player player2 = players.get(1);

        Card card1 = new Card(Card.Suit.DIAMOND, Card.Rank.ACE);
        Card card2 = new Card(Card.Suit.SPADE, Card.Rank.ACE);

        player1.addCard(card1);
        player2.addCard(card2);

        // Play card to move it to played cards
        player1.playCard(true);
        player2.playCard(true);

        Player maxPlayer = Collections.max(players,new PlayerFirstCardComparator());

        assertEquals(player1.getFirstPlayedCardValue(),maxPlayer.getFirstPlayedCardValue());
        assertEquals(player2.getFirstPlayedCardValue(),maxPlayer.getFirstPlayedCardValue());
    }

    /**
     * Compare when no players have a played card.
     *
     * Result: The max player card value will be 0,
     *     since the card value is 0 when there is no played card
     */
    @Test
    public void testCompareFirstCardTwoPlayersNoCards() {

        Player maxPlayer = Collections.max(players,new PlayerFirstCardComparator());

        assertEquals(0,maxPlayer.getFirstPlayedCardValue());
    }

    /**
     * Compare when player 2 has no played card and player 1 has one.
     *
     * Result: The max player card value will be the same player 1's card value
     */
    @Test
    public void testCompareFirstCardTwoPlayersFirstPlayerNoCards() {

        Player player1 = players.get(0);

        Card card1 = new Card(Card.Suit.DIAMOND, Card.Rank.ACE);

        player1.addCard(card1);

        // Play card to move it to played cards
        player1.playCard(true);

        Player maxPlayer = Collections.max(players,new PlayerFirstCardComparator());

        assertEquals(player1.getFirstPlayedCardValue(),maxPlayer.getFirstPlayedCardValue());

    }
    /**
     * Compare when player 1 has no played card and player 2 has one.
     *
     * Result: The max player card value will be the same player 2's card value
     */
    @Test
    public void testCompareFirstCardTwoPlayersSecondPlayerNoCards() {

        Player player2 = players.get(1);

        Card card2 = new Card(Card.Suit.DIAMOND, Card.Rank.ACE);

        player2.addCard(card2);

        // Play card to move it to played cards
        player2.playCard(true);

        Player maxPlayer = Collections.max(players,new PlayerFirstCardComparator());

        assertEquals(player2.getFirstPlayedCardValue(),maxPlayer.getFirstPlayedCardValue());

    }
}

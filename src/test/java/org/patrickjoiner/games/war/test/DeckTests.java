package org.patrickjoiner.games.war.test;

import org.junit.Test;
import org.patrickjoiner.games.war.Card;
import org.patrickjoiner.games.war.Deck;
import org.patrickjoiner.games.war.Player;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
/**
 * Tests for Deck
 */
public class DeckTests {

    /**
     * Create a deck of cards.
     *
     * Result:  Deck should not be null and should have 52 cards.
     */
    @Test
    public void testDeckNumberOfCards() {
        Deck deck = new Deck();
        assertNotNull(deck);
        assertEquals(52,deck.numberOfCards());
    }

    /**
     * Remove a card from a new deck.
     *
     * Result:  Deck should have 51 cards after removing a card.
     */
    @Test
    public void testDeckRemoveCard() {
        Deck deck = new Deck();
        assertEquals(52,deck.numberOfCards());
        Card card = deck.removeCard();
        assertEquals(51,deck.numberOfCards());
        assertNotEquals(card,null);
    }

    /**
     * Clear the cards in the deck
     *
     * Result:  Deck should have zero cards.
     */
    @Test
    public void testDeckClearCards() {
        Deck deck = new Deck();
        assertEquals(deck.numberOfCards(),52);
        deck.clearCards();
        assertEquals(0,deck.numberOfCards());
    }


    /**
     * Try to remove a card from an empty deck
     *
     * Result:  Returned card should be null, and deck should remain with 0 cards.
     */
    @Test
    public void testDeckRemoveCardFromEmptyDeck() {
        Deck deck = new Deck();
        assertEquals(52,deck.numberOfCards());

        Card card;
        while (deck.numberOfCards() > 0) {
            card = deck.removeCard();
            assertNotNull(card);
        }
        assertEquals(0,deck.numberOfCards());

        card = deck.removeCard();
        assertEquals(0,deck.numberOfCards());
        assertNull(card);
    }

    /**
     * Deal to a null player list
     *
     * Result:  Number of cards in deck should remain the same (52).
     */
    @Test
    public void testDeckDealToNullPlayers() {
        Deck deck = new Deck();
        assertEquals(52,deck.numberOfCards());

        deck.dealCards(null, true);
        assertEquals(52,deck.numberOfCards());
    }

    /**
     * Deal to an empty player list
     *
     * Result:  Number of cards in deck should remain the same (52).
     */
    @Test
    public void testDeckDealToEmptyPlayers() {
        Deck deck = new Deck();
        assertEquals(52,deck.numberOfCards());

        deck.dealCards(new ArrayList<>(), true);
        assertEquals(52,deck.numberOfCards());
    }

    /**
     * Deal to a list of one player. Normally this should not
     *    occur since one player is not allowed, but just testing
     *    to make sure there is no issue.
     *
     * Result:  Number of cards in deck should remain the same (52)
     *     since having only one player is not allowed.
     */
    @Test
    public void testDeckDealToOnePlayer() {
        Deck deck = new Deck();
        assertEquals(52,deck.numberOfCards());

        List<Player> players = new ArrayList<>();
        players.add(new Player(1));
        deck.dealCards(players, true);
        assertEquals(52,deck.numberOfCards());
    }

    /**
     * Deal to a list of two players
     *
     * Result:  Number of cards in deck should be zero.
     */
    @Test
    public void testDeckDealToTwoPlayers() {
        Deck deck = new Deck();
        assertEquals(52,deck.numberOfCards());

        List<Player> players = new ArrayList<>();
        players.add(new Player(1));
        players.add(new Player(2));

        deck.dealCards(players,true);
        assertEquals(0,deck.numberOfCards());
    }

    /**
     * Deal to a list of three players
     * Result:  Number of cards in deck should remain the same (52) since 52 is not divisible by 3.
     */
    @Test
    public void testDeckDealToThreePlayers() {
        Deck deck = new Deck();
        assertEquals(52,deck.numberOfCards());

        List<Player> players = new ArrayList<>();
        players.add(new Player(1));
        players.add(new Player(2));
        players.add(new Player(3));

        deck.dealCards(players,true);
        assertEquals(52,deck.numberOfCards());
    }

    /**
     * Deal to two players from an empty deck.
     *
     * Result:  The deck should remain empty (i.e. no error)
     */
    @Test
    public void testDeckDealToTwoPlayersFromEmptyDeck() {
        Deck deck = new Deck();
        deck.clearCards();

        List<Player> players = new ArrayList<>();
        players.add(new Player(1));
        players.add(new Player(2));

        deck.dealCards(players,true);
        assertEquals(0,deck.numberOfCards());
    }

}

package org.patrickjoiner.games.war.test;

import org.junit.Before;
import org.junit.Test;
import org.patrickjoiner.games.war.*;
import org.patrickjoiner.games.war.round.Round;
import org.patrickjoiner.games.war.round.WarRound;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Tests for WarRound
 */
public class WarRoundTests {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private List<Player> twoPlayers;
    private List<Card> discardPile;


    @Before
    public void testSetup() {
        twoPlayers = new ArrayList<>();
        twoPlayers.add(new Player(1));
        twoPlayers.add(new Player(2));

        discardPile = new ArrayList<>();
        Card card = new Card(Card.Suit.DIAMOND, Card.Rank.THREE);
        discardPile.add(card);

        card = new Card(Card.Suit.SPADE,Card.Rank.TWO);
        discardPile.add(card);

    }

    /**
     * First player wins the first card in war.
     * Result:  First player gets 6 cards (2 from discard pile + 4 from war)
     */
    @Test
    public void testWarRoundFirstCardWin() {
        Player player1 = twoPlayers.get(0);
        Player player2 = twoPlayers.get(1);

        Card card1 = new Card(Card.Suit.CLUB, Card.Rank.ACE);
        Card card2 = new Card(Card.Suit.DIAMOND, Card.Rank.TEN);
        player1.addCard(card1);
        player2.addCard(card2);

        card1 = new Card(Card.Suit.HEART, Card.Rank.TWO);
        card2 = new Card(Card.Suit.SPADE, Card.Rank.TEN);
        player1.addCard(card1);
        player2.addCard(card2);

        Round round = new WarRound(twoPlayers, discardPile);
        round.play();

        // player 1 will win, so they will have 4 cards from war + 2 incoming cards = 6 cards
        assertEquals(6,player1.numberOfUnplayedCards());

        // player 2 will have no cards.
        assertEquals(0,player2.numberOfUnplayedCards());

        // discard pile will still have 2 cards from the original round
        assertEquals(0,round.getDiscardPile().size());
    }


    /**
     * First card in war is a tie, but player 2 wins second card
     * Result:  Player 2 should get 6 cards (2 in discard pile, 4 from war)
     */
    @Test
    public void testWarRoundFirstCardTie() {
        Player player1 = twoPlayers.get(0);
        Player player2 = twoPlayers.get(1);

        Card card1 = new Card(Card.Suit.CLUB, Card.Rank.ACE);
        Card card2 = new Card(Card.Suit.DIAMOND, Card.Rank.ACE);
        player1.addCard(card1);
        player2.addCard(card2);

        card1 = new Card(Card.Suit.HEART, Card.Rank.TWO);
        card2 = new Card(Card.Suit.SPADE, Card.Rank.TEN);
        player1.addCard(card1);
        player2.addCard(card2);

        WarRound round = new WarRound(twoPlayers, discardPile);
        round.play();

        // player 1 will have no cards.
        assertEquals(0,player1.numberOfUnplayedCards());

        // player 2 will win, so they will have 4 cards from war + 2 incoming cards = 6 cards
        assertEquals(6,player2.numberOfUnplayedCards());

        // discard pile will still have 2 cards from the original round
        assertEquals(0,round.getDiscardPile().size());


    }

    /**
     * Both cards tie in a war round
     * Result:  Discard pile should have 6 cards (2 from discard pile + 4 from war)
     */
    @Test
    public void testWarRoundBothCardsTie() {
        Player player1 = twoPlayers.get(0);
        Player player2 = twoPlayers.get(1);

        Card card1 = new Card(Card.Suit.CLUB, Card.Rank.ACE);
        Card card2 = new Card(Card.Suit.DIAMOND, Card.Rank.ACE);
        player1.addCard(card1);
        player2.addCard(card2);

        card1 = new Card(Card.Suit.HEART, Card.Rank.TWO);
        card2 = new Card(Card.Suit.SPADE, Card.Rank.TWO);
        player1.addCard(card1);
        player2.addCard(card2);

        WarRound round = new WarRound(twoPlayers, discardPile);
        round.play();

        // player 1 will have no cards.
        assertEquals(0,player1.numberOfUnplayedCards());

        // player 2 will win, so they will have 4 cards from war + 2 incoming cards = 6 cards
        assertEquals(0,player2.numberOfUnplayedCards());

        // discard pile will still have 2 cards from the original round
        assertEquals(6,round.getDiscardPile().size());
    }

    /**
     * One player has zero cards left
     * Result:  Other player should win all 6 cards
     */
    @Test
    public void testWarRoundOnePlayerOutWithZeroCards() {
        Player player1 = twoPlayers.get(0);
        Player player2 = twoPlayers.get(1);

        Card card1 = new Card(Card.Suit.CLUB, Card.Rank.ACE);
        Card card2 = new Card(Card.Suit.SPADE, Card.Rank.TEN);

        // player 1 has 2 cards , player 2 0
        player1.addCard(card1);
        player1.addCard(card2);

        WarRound round = new WarRound(twoPlayers, discardPile);
        round.play();

        // player 1 will get 2 cards from the discard pile + 2 that they played
        assertEquals(4,player1.numberOfUnplayedCards());

        // player 2 will have no cards since they didn't have any
        assertEquals(0,player2.numberOfUnplayedCards());

        // discard pile will have no cards
        assertEquals(0,round.getDiscardPile().size());


    }
    /**
     * One player has one card left
     * Result:  Other player should win all 5 cards
     */
    @Test
    public void testWarRoundOnePlayerOutWithOneCard() {
        Player player1 = twoPlayers.get(0);
        Player player2 = twoPlayers.get(1);

        Card card1 = new Card(Card.Suit.CLUB, Card.Rank.ACE);
        Card card2 = new Card(Card.Suit.SPADE, Card.Rank.TEN);
        Card card3 = new Card(Card.Suit.DIAMOND, Card.Rank.QUEEN);

        // player 1 has 2 cards , player 2 0
        player1.addCard(card1);
        player1.addCard(card2);
        player2.addCard(card3);

        WarRound round = new WarRound(twoPlayers, discardPile);
        round.play();

        // player 1 will get 2 cards from the discard pile + 2 that they played
        assertEquals(5,player1.numberOfUnplayedCards());

        // player 2 will have no cards since they didn't have any
        assertEquals(0,player2.numberOfUnplayedCards());

        // discard pile will have no cards
        assertEquals(0,round.getDiscardPile().size());


    }

}


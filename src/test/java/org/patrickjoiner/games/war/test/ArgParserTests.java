package org.patrickjoiner.games.war.test;

import org.junit.Test;
import org.patrickjoiner.games.war.ArgParser;

import static org.junit.Assert.*;

/**
 * Tests for ArgParser
 */
public class ArgParserTests {

    /**
     * Positive number of players (4)
     *
     * Result: parsed argument should be 4
     */
    @Test
    public void testArgParserWithPositivePlayers() {
        ArgParser argParser = new ArgParser("-p","4");
        assertNotNull(argParser.getNumberOfPlayers());
        assertEquals(4, argParser.getNumberOfPlayers().intValue());
    }

    /**
     * Zero players
     *
     * Result: parsed argument should be zero
     */
    @Test
    public void testArgParserWithZeroPlayers() {
        ArgParser argParser = new ArgParser("-p","0");
        assertNotNull(argParser.getNumberOfPlayers());
        assertEquals(0, argParser.getNumberOfPlayers().intValue());
    }

    /**
     * Negative number of players (-1)
     *
     * Result: parsed argument should be -1
     */
    @Test
    public void testArgParserWithNegativePlayers() {
        ArgParser argParser = new ArgParser("-p","-1");
        assertNotNull(argParser.getNumberOfPlayers());
        assertEquals(-1, argParser.getNumberOfPlayers().intValue());
    }

    /**
     * Unexpected extra parameter
     *
     * Result: number of players should be null since parse will fail.
     */
    @Test
    public void testArgParserWithUnexpectedParameter() {
        ArgParser argParser = new ArgParser("-p","2","-t","5");
        assertNull(argParser.getNumberOfPlayers());
    }

    /**
     * Parse an argument which is not a number
     *
     * Result: number of players should be null since parse will fail.
     */
    @Test
    public void testArgParserWithBadValue() {
        ArgParser argParser = new ArgParser("-p","xxx");
        assertNull(argParser.getNumberOfPlayers());
    }

    /**
     * Parse a missing argument
     *
     * Result: number of players should be null since parse will fail.
     */
    @Test
    public void testArgParserWithMissingValue() {
        ArgParser argParser = new ArgParser("-p");
        assertNull(argParser.getNumberOfPlayers());
    }

    /**
     * Parse no arguments. This should not parse.
     *
     * Result: number of players should be null since parse will fail.
    */
     @Test
    public void testArgParserWithNoValue()  {
        ArgParser argParser = new ArgParser();
        assertNull(argParser.getNumberOfPlayers());
    }
}

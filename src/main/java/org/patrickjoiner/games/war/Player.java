package org.patrickjoiner.games.war;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Player represents a player in the game.
 */
public class Player {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private List<Card> unplayedCards;
    private List<Card> playedCards;
    private int number = 0;

    /**
     * Default constructor; default player
     *    number is 1
     */
    public Player() {
        initPlayer(1);
    }

    /**
     * Constructor with player number
     * @param number the player number (e.g. player 1, player 2, player 3).
     */
    public Player(int number) {
        initPlayer(number);
    }

    /**
     * Initialize the player with player number.
     * Create blank arrays for cards and played cards.
     * Played cards are cards played in a round.
     *
     * @param number the number for the player (e.g. player 1, player 2, etc.)
     */
    private void initPlayer(int number) {
        this.number = number;
        unplayedCards = new ArrayList<>(); // unplayed cards
        playedCards = new ArrayList<>();  // cards in play
    }

    /**
     * Add a card to the player's cards
     *
     * @param card the card to add
     *
     */
    public void addCard(Card card) {
        if (unplayedCards!=null && card!=null) {
            unplayedCards.add(card);
        }
    }
    /**
     * Get the number of cards remaining
     *
     * @return int the number of cards remaining
     *
     */
    int numberOfCards() {
        return this.numberOfPlayedCards() + this.numberOfUnplayedCards();
    }
    /**
     * Get the number of cards remaining
     *
     * @return int the number of cards remaining
     *
     */
    public int numberOfUnplayedCards() {
        return unplayedCards == null ? 0 : unplayedCards.size();
    }

    /**
     * Remove a card
     *
     * @return Card the card that was removed or null if no cards.
     *
     */
    public Card removeCard() {

        if (this.unplayedCards == null || this.unplayedCards.size()==0) {
            return null;
        } else {
            return unplayedCards.remove(0);
        }
    }

     /**
      * Play a card, showing either the face or back of the card, including the title.
      *
      * @param showCardFace a boolean value indicating if the face of the card should be shown.
      *
      */
    public void playCard(boolean showCardFace) {  // add card to bottom of played cards.
        playCard(showCardFace,true);
    }

    /**
     * Play a card, showing either the face or back of the card
     *
     * @param showCardFace a boolean value indicating if the face of the card should be shown.
     * @param showTitle a boolean value indicating if the title (player number and number of cards) should be shown
     */
    public void playCard(boolean showCardFace, boolean showTitle) {  // add card to bottom of played cards.
        if (unplayedCards.size()>0) {
            playedCards.add(unplayedCards.remove(0));

            if (showTitle) {
                System.out.println(
                        String.format("Player %d (%d card(s))",
                                this.number,
                        this.numberOfCards()));
            }

            if (this.getFirstPlayedCard()!=null) {
                System.out.println(this.getFirstPlayedCard().getAsciiImage(showCardFace));
            } else {
                logger.error("First played card was null");
            }
        }
    }

    /**
     * Remove the played cards
     *
     * @return a List of the Card objects that were removed
     *
     */
    public List<Card> removePlayedCards() {
        List<Card> oldCards = new ArrayList<>();
        oldCards.addAll(this.playedCards);
        this.playedCards.clear();
        return oldCards;
    }
    /**
     * Remove the unplayed cards
     *
     * @return a List of the unplayed Card objects that were removed
     *
     */
    public List<Card> removeUnplayedCards() {
        List<Card> oldCards = new ArrayList<>();
        oldCards.addAll(this.unplayedCards);
        this.unplayedCards.clear();
        return oldCards;
    }
/*
    public String cardString() {
        return String.join(" ",cards.stream().map(c -> c.toString()).collect(Collectors.toList()));
    }
*/

    /**
     * Remove the first card that was played
     *
     * @return Card the card that was played
     *
     */
    public Card removeFirstPlayedCard() {
        if (this.playedCards!=null && this.playedCards.size()>0) {
            return this.playedCards.remove(0);
        } else {
            return null;
        }
    }

    /**
     * Remove all cards
     *
     */
    public void removeCards() {
        this.unplayedCards.clear();
    }

    /**
     * Get the number of played cards
     *
     * @return int the number of played cards
     */
    public int numberOfPlayedCards() {
        return this.playedCards==null ? 0 : this.playedCards.size();
    }

    /**
     * Add cards to the player's cards, optionally shuffling them.
     *
     * @param cardsToAdd the cards to add
     * @param shuffle a true/false value indicating if the cards should be shuffled
     *
     */
    public void addCards(List<Card> cardsToAdd, boolean shuffle) {
        if (cardsToAdd!=null && this.unplayedCards!=null) {
            // shuffle cards
            if (shuffle) {
                Collections.shuffle(cardsToAdd);
            }
            logger.info("Adding {} cards to player {}, currently {}",cardsToAdd.size(),this.getNumber(), this.unplayedCards.size());
            this.unplayedCards.addAll(cardsToAdd);
            cardsToAdd.clear();
            logger.info("Player cards: {}, discarded: {}",this.unplayedCards.size(),cardsToAdd.size());
        }
    }
    /**
     * Show the first played card
     *
     * Outputs the string representing the card to stdout
     *
     */
    public void showFirstPlayedCard() {
        Card card = this.getFirstPlayedCard();
        if (card!=null) {
            System.out.println(String.format("Player %d:%n%s", this.number,
                    card.getAsciiImage(true)));
        } else {
            logger.error("Could not show first card because it was null!");
        }
    }

    /**
     * Get the first played card
     *
     * @return Card the first played card
     *
     */
    private Card getFirstPlayedCard() {
        if (playedCards!=null && playedCards.size() > 0) {
            return this.playedCards.get(0);
       } else {
           return null;
        }
    }
    /**
     * Get the int value of the first played card
     *
     * @return int the value of the first played card, or 0 if there is no first card.
     *
     */
    public int getFirstPlayedCardValue() {
        if (playedCards!=null && playedCards.size() > 0) {
            return this.playedCards.get(0).getRank().value;
        } else {
            return 0;
        }
    }

    /**
     * Get the played cards
     *
     * @return a List of played Card objects
     *
     */
    public List<Card> getPlayedCards() {
        return playedCards;
    }


    /**
     * Get the player number
     *
     * @return int the player number
     *
     */
    public int getNumber() {
        return number;
    }

}

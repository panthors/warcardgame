package org.patrickjoiner.games.war;

import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An ArgParser parses a String vararg (usually from the command line)
 *    and exposes a method to get the number of players from the command line
 *    arguments.
 */
public class ArgParser {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private CommandLine cmd;

    public ArgParser(String... args) {
        Options options = new Options();
        CommandLineParser parser = new DefaultParser();

        final Option fileOption = Option.builder("p")
                .argName("players")
                .hasArg()
                .required()
                .desc("Number of players")
                .build();
        options.addOption(fileOption);
        try {
            cmd = parser.parse(options,args);
        } catch (Exception e) {
            cmd = null;
        }
    }

    /**
     * Get the number of players
     *
     * @return the number of players or null if the number couldn't be determined
     */
    public Integer getNumberOfPlayers() {
        Integer numPlayers = null;

        if (cmd != null && cmd.hasOption("p")) {
            String optionValue = cmd.getOptionValue("p");
            if (optionValue!=null &&  !optionValue.trim().isEmpty())
                try {
                    numPlayers = Integer.parseInt(optionValue);
                } catch (NumberFormatException nfe) {
                    logger.info(String.format("Couldn't parse number of players: %s",optionValue));
                }
            else {
                logger.info(String.format("Invalid argument for -p: %s",optionValue));
            }
        } else {
            logger.info("Number of players not specified or unavailable");
        }
        return numPlayers;
    }

}

package org.patrickjoiner.games.war;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Deck represents a (standard) 52-card deck of cards. It is not generic enough to handle all types of card games.
 */
public class Deck {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private List<Card> cards;


    /**
     * Create a deck of cards. The number of cards is
     *    (number of suits * number of ranks)
     */
    public Deck() {
        cards = new ArrayList<>();
        Card card;
        for (Card.Suit suit: Card.Suit.values()) {
            for (Card.Rank rank: Card.Rank.values()) {
                card = new Card(suit, rank);
                cards.add(card);
            }

        }
    }

    /**
     * Shuffle the deck. This is just a wrapper around
     *   Collections.shuffle so we will not specifically test the shuffle function.
     */
    private void shuffle() {
        if (cards!=null) {
            Collections.shuffle(cards);
        }
    }

    /**
     * Get the number of cards in the deck
     *
     *   @return an int representing the number of cards in the deck.
     */
    public int numberOfCards() {
        return cards==null ? 0 : cards.size();
    }


    /**
     *  Remove a card from the deck.
     *
     *   @return the first Card in the deck, or null if the deck is empty.
     */
    public Card removeCard() {
        if (cards!=null && cards.size() > 0) {
            return cards.remove(0);
        } else {
            return null;
        }
    }

    /**
     * Clear all the cards from the deck
     */
    public void clearCards() {
        if (cards!=null && cards.size() > 0) {
            cards.clear();
        }
    }

    /**
     * Deal the cards to the players in the provided List.
     *    If the number of cards is not divisible
     *    by the number of players, or if the number of players
     *    is less than two, don't deal any cards.
     *
     *    @param players  A List of Player objects to which the cards should be dealt.
     *    @param shuffleCards  a boolean indicating if the cards should be shuffled before adding.
     */
    public void dealCards(List<Player> players, boolean shuffleCards) {
        if (this.numberOfCards()==0) {
            logger.error("Cannot deal cards: no cards in the deck.");
            return;
        }
        if (players == null) {
            logger.error("Cannot deal cards; the list of players was null.");
            return;
        }
        if (players.size() < 2) {
            logger.error("Cannot deal cards; the number of players was less than 2.");
            return;
        }
        if (cards.size() % players.size() != 0) {
            logger.error("Could not deal {} cards to {} players evenly.", cards.size(), players.size());
            return;
        }

        // shuffle cards if necessary
        if (shuffleCards) {
            this.shuffle();
        }

        int cardsPerPlayer = cards.size() / players.size();
        for (int i = 0; i < cardsPerPlayer; i++) {
            for (Player player : players) {
                player.addCard(cards.remove(0));
            }
        }

    }
}

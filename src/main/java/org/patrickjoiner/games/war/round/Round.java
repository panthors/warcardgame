package org.patrickjoiner.games.war.round;

import org.patrickjoiner.games.war.Card;
import org.patrickjoiner.games.war.Player;
import org.patrickjoiner.games.war.PlayerFirstCardComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Round defines abstract methods for gameplay and has some utility methods shared by subclasses.
 */
public abstract class Round {

    protected List<Player> players; // the players in this round
    List<Card> discardPile;  // the discard pile for the round

    /**
     * Abstract method for playing the round
     */
    public abstract void play();

    /**
     * Abstract method for playing cards for the round
     */
    protected abstract void playCards();

    /**
     * Abstract method for printing the message at the start of the round
     */
    protected abstract void printRoundMessage();

    /**
     * Abstract method for rewarding the winner of a round
     */
    protected abstract void rewardWinner();

    /**
    *  Get the maximum card value for all players, ignoring players
    *     that didn't play any cards. Since the suits are ignored,
    *     just look at the rank value. Uses PlayerFirstCardComparator.
    *
    *  @return  an int with the max value, or 0 if there is no max
    */
    private int getMaxPlayerCardValue() {
        int maxValue = 0;
        if (players!=null) {
            Player maxPlayer = Collections.max(players, new PlayerFirstCardComparator());
            if (maxPlayer!=null) {
                maxValue = maxPlayer.getFirstPlayedCardValue();
            }
        }
        return maxValue;
    }
    /**
     *  Initialize the round with the players
     *
     *  @param roundPlayers a List of Player objects for the round
     *  @param discardPile  a List of Card objects representing the discard pile
     */
    void initRound(List<Player> roundPlayers,List<Card> discardPile) {

        this.discardPile = new ArrayList<>();
        this.players = new ArrayList<>();

        if (discardPile!=null) {
            this.discardPile.addAll(discardPile);
        }

        if (roundPlayers!=null) {
            this.players.addAll(roundPlayers);
        }
    }
    /**
     *  Get the number of players in the round.
     *     If players is null then return 0.
     *
     *  @return    an int with the number of players
     */
    public int numberOfPlayers() {
         if (players==null) {
             return 0;
         } else {
             return players.size();
         }
    }

    /**
     *  Get the first Player in the players list.
     *
     *  @return  the first Player in the list, or null if there is none
     */
    Player getFirstPlayer() {
        if (players==null || players.size()==0) {
            return null;
        } else {
            return players.get(0);
        }
    }

    /**
     *  Get the players for this round.
     *
     *  @return  a List of Player objects
     */
    public List<Player> getPlayers() {
        return players;
    }

    /**
     *  Get the discarded cards for this round.
     *
     *  @return  a List of Card objects representing the discarded cards.
     */
    public List<Card> getDiscardPile() {
        return discardPile;
    }



    /**
     *  After the round has been played, remove players
     *     that either didn't play a card or whose card did not match
     *     the top card. Before removing them, add their cards to
     *     the discarded pile.
     *
     */
    void removeLosers() {

        int topCardValue = getMaxPlayerCardValue();

        Iterator<Player> playerIterator = players.iterator();
        while (playerIterator.hasNext()) {
            Player p = playerIterator.next();
            if (p.numberOfPlayedCards() == 0 || p.getFirstPlayedCardValue() != topCardValue) {
                // losers discard all cards
                discardPile.addAll(p.removePlayedCards());
                playerIterator.remove();
            } else {
                // winners only discard the first card since
                //   they may need to play another card
                discardPile.add(p.removeFirstPlayedCard());
            }
        }
    }

}

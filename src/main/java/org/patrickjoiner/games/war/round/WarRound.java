package org.patrickjoiner.games.war.round;

import org.patrickjoiner.games.war.Card;
import org.patrickjoiner.games.war.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
/**
 * WarRound extends Round and handles gameplay for a war round (i.e. two cards played per player).
 */
public class WarRound extends Round {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     *  Default constructor has null players and null discarded cards
     */
    public WarRound() {
        initRound(null,null);
    }

    /**
     *  Constructor with players List and discarded card List
     *
     *  @param warPlayers a List of Player objects for the round
     *  @param discardPile  a List of Card objects representing the discard pile
     */
    public WarRound(List<Player> warPlayers, List<Card> discardPile) {
        initRound(warPlayers,discardPile);
    }

    /**
     *  Play cards for each player
     */
    protected void playCards() {
        // play 2 cards
        for (Player player : players) {
            player.playCard(false);
            player.playCard(false, false);
        }
    }


    /**
     *  Remove players that don't have enough cards
     *    to play war.
     */
    private void removeUnqualifiedPlayers() {
        Iterator<Player> playerIterator = players.iterator();
        while (playerIterator.hasNext()) {

            Player player = playerIterator.next();
            // if player doesn't have 2 cards they lose the war; remove their cards and remove from war

            if (player.numberOfUnplayedCards() < 2) {
                System.out.println(String.format("Player %d doesn't have 2 cards (only %d) so cannot join the war",player.getNumber(), player.numberOfUnplayedCards()));
                this.discardPile.addAll(player.removeUnplayedCards());
                playerIterator.remove();
            }
        }
    }

    /**
     *  Show cards for each player
     */
    private void showCards() {
        for (Player player : players) {
            player.showFirstPlayedCard();
        }
    }

    /**
     *  Play a war round, i.e. a round with two cards per player.
     *    Each player plays two cards; if a player doesn't have
     *       two cards then they lose.
     */
    public void play() {

        this.printRoundMessage();

        removeUnqualifiedPlayers();

        if (players.size() >= 1) { // enough players for war
            playCards();

            System.out.println("War card 1:");
            showCards();  // show first card
            removeLosers(); // remove losers

            if (players.size() > 1) {
                System.out.println("War card 2:");
                showCards();  // show second card
                removeLosers(); // remove losers
            }

            if (players.size() == 1) {
                rewardWinner();
            } else {
                logger.info(String.format("Number of players after war: %d",players.size()));
            }
        } else {
            System.out.println("No players are able to play war.");
        }
    }

    protected void rewardWinner() {
        if (players!=null && players.size()==1) {
            // if one winner, he/she gets all the cards
            Player winner = getFirstPlayer();

            // add winners cards to discard pile; they will get them back
            this.discardPile.addAll(winner.removePlayedCards());

            System.out.println(String.format("Player %d wins the war and %d cards", winner.getNumber(), discardPile.size()));
            winner.addCards(this.discardPile, true);
        }
    }

    protected void printRoundMessage() {
        System.out.println();
        if (numberOfPlayers()>0) {
            System.out.println(String.format("%nWAR! (players %s)%n",
                    this.players.stream().map(p->String.valueOf(p.getNumber()))
                            .collect(Collectors.joining(", "))));
        }
        System.out.println();
    }

}

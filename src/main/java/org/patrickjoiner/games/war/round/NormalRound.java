package org.patrickjoiner.games.war.round;

import org.patrickjoiner.games.war.Card;
import org.patrickjoiner.games.war.Player;
import java.util.List;

/**
 * NormalRound extends Round and handles gameplay for a normal round (i.e. one card played per player).
 */
public class NormalRound extends Round {

    private int roundNumber = 0;

    /**
     *  Default constructor has null players and discard pile
     */
    public NormalRound() {

        initRound(1,null, null);
    }

    /**
     *  Constructor with players List and discard pile
     *
     *  @param roundNumber the number of the round
     *  @param players a List of Player objects for the round
     *  @param discardPile  a List of Card objects representing the discard pile
     */
    public NormalRound(int roundNumber, List<Player> players, List<Card> discardPile) {
        initRound(roundNumber, players, discardPile);
    }

    /**
     *  Setup; set roundNumber and call parent initRound
     *
     *  @param roundNumber the number of the round
     *  @param players a List of Player objects for the round
     *  @param discardPile  a List of Card objects representing the discard pile
     */
    private void initRound(int roundNumber, List<Player> players, List<Card> discardPile) {
        super.initRound(players,discardPile);
        this.roundNumber=roundNumber;
     }

    /**
     *  Play a normal round, i.e. a round with one card per player.
     *    Each player plays a card, then losers are removed and
     *    if there is a winner (one player left),
     *    the discarded cards are added to the winner's pile.
     */
    public void play() {
        // print round message
        this.printRoundMessage();

        // play cards for each player
        this.playCards();

        // remove losers and add their cards to discarded pile
        this.removeLosers();

        // give cards to the winner (if there was one)
        this.rewardWinner();
    }

    /**
     *  Reward winner (if there is one) by
     *     giving them all of the discarded cards
     */
    public void rewardWinner() {
        if (players!=null && players.size()==1) {
            Player winner = getFirstPlayer();
            System.out.println(String.format("Player %d wins %d cards", winner.getNumber(),discardPile.size()));
            // add discarded cards to winner
            winner.addCards(discardPile,true);
        }
    }


    /**
     * Print message about round number to stdout.
     */
    protected void printRoundMessage() {
        System.out.println();
        System.out.println("*****************************");
        System.out.println(String.format("Round %d", this.roundNumber));
        System.out.println("*****************************");

    }

    /**
     * Get the round number
     * @return an int representing the round number
     */
    public int getRoundNumber() {
        return roundNumber;
    }

    /**
     *  Play a card for each player
     */
    protected void playCards() {
        if (players!=null) {
            for (Player player: players) {
                player.playCard(true);
            }
        }
    }

}

package org.patrickjoiner.games.war;

import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

/**
 * The main entry point for the application. Parses the command line arguments and plays the game if it is ready.
 */
@SpringBootApplication
public class WarCardGameApplication implements CommandLineRunner {
    private final String APP_NAME = "war-card-game-X.Y-RELEASE.jar";

	public static void main(String[] args) {
        SpringApplication.run(WarCardGameApplication.class, args);
	}

    @Override
	public void run(String... args) {
    	ArgParser argParser = new ArgParser(args);
		Integer numberOfPlayers = argParser.getNumberOfPlayers();

		if (numberOfPlayers == null) {
            System.err.println("Usage: java -jar " + APP_NAME + " -p <number of players>");
            System.err.println();
			System.exit(1);
		}

		Game game = new Game(numberOfPlayers);
        if (!game.isReady()) {
            System.exit(1);
        }
		game.play();
	}
}

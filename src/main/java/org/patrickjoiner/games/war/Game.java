package org.patrickjoiner.games.war;

import org.patrickjoiner.games.war.round.NormalRound;
import org.patrickjoiner.games.war.round.Round;
import org.patrickjoiner.games.war.round.WarRound;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.*;

/**
 * Game represents a game of war. It has properties for players and a deck.
 */
public class Game {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private List<Player> players;
    private Deck deck;
    private boolean ready = false;


    /**
     * Create a game with a specific number of players.
     * The number of players must be greater than one,
     * and the number of cards must be divisible by the number
     * of players, otherwise not every player will get the same
     * number of cards.
     *
     * @param numberOfPlayers and int value representing the number of players in the game
     */
    public Game(int numberOfPlayers) {
        logger.info("Creating game with {} players...", numberOfPlayers);
        setupGame(numberOfPlayers);
    }

    /**
     * Return a boolean indicating if the game is ready to be played.
     *
     * @return a boolean indicating if the game is ready.
     */
    public boolean isReady() {
       return this.ready;
    }

    
    /**
     * Add numberOfPlayers players to the player list.
     * 
     * @param numberOfPlayers the number of players to add
     */
    private void setupGame(int numberOfPlayers) {
        deck = new Deck();

        if (isNumberOfPlayersValid(numberOfPlayers)) {
            players = new ArrayList<>();
            for (int i = 0; i < numberOfPlayers; i++) {
                players.add(new Player(i + 1));
            }
            if (players.size()==numberOfPlayers) {
                this.ready=true;
            } else {
                System.err.println("There was a problem setting up players; aborting game.");
                System.err.println();
            }
        }
    }

    /**
     * Return the number of players in the game.
     *
     * @return the number of players
     */
    public int numberOfPlayers() {
        if (players == null) {
            return 0;
        } else {
            return players.size();
        }
    }

    private boolean isNumberOfPlayersValid(int numberOfPlayers) {

        if (numberOfPlayers < 2) {
            System.err.println("There must be at least 2 players; aborting game.");
            System.err.println();
            return false;
        }
        
        if (deck.numberOfCards() % numberOfPlayers != 0) {
            System.err.println(String.format("Cannot deal %d cards to %d players evenly; aborting game.",deck.numberOfCards(),numberOfPlayers));
            System.err.println();
            return false;
        }

        return true;
    }

    /**
     * Play the game.
     */
    public void play() {

        NormalRound round;
        int roundNumber = 1;
        List<Card> discardPile = new ArrayList<>();

        this.deck.dealCards(this.players, true);

        // main loop
        while (players.size() > 1) {
            round = new NormalRound(roundNumber,players,discardPile);
            round.play();

            discardPile = round.getDiscardPile();

            // if more than 1 player left after a normal round, WAR
            if (round.numberOfPlayers() > 1) {
                // reset discard pile in case there is a tie
               Round warRound;
                do {
                    warRound = new WarRound(round.getPlayers(), discardPile);
                    warRound.play();
                    // reset discard pile
                    discardPile = warRound.getDiscardPile();
                } while(warRound.getPlayers().size()>1);
            }

            // remove any players who are out of cards
            this.removePlayersWithoutCards();

            // update the round number
            roundNumber++;
        }
        this.printWinnerMessage();

    }


    /**
     * Print a message about the winner (if any) to stdout.
     */
    private void printWinnerMessage() {
        System.out.println();

        if (players.size() == 1) {
            Player winner = players.get(0);
            if (winner.numberOfCards()==52) {
                System.out.println(String.format("Player %d wins the game with %d cards!", winner.getNumber(), winner.numberOfUnplayedCards()));
            } else {
                System.out.println("There was no winner; no player got all of the cards!");
            }
        } else {
            System.out.println("There was no winner!");
        }
        System.out.println();
        System.out.println();
    }


    /**
     * Remove players with no cards, since they can't play any more.
     */
    private void removePlayersWithoutCards() {
        // remove players who are out of cards
        players.removeIf((Player p) -> p.numberOfUnplayedCards() == 0);
    }

}

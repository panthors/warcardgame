package org.patrickjoiner.games.war;

import java.io.Serializable;
import java.util.Comparator;

/**
 * A comparator for comparing the first played card value for two Player objects.
 */
public class PlayerFirstCardComparator implements Comparator<Player>, Serializable {

    /**
     * Compare the values of the first played card of two players.
     *
     * @return  &lt; 1 if p1 &gt; p2, 0 if p1 = p2, and &gt; 1 if p1 &lt; p2
     */
    @Override
    public int compare(Player p1, Player p2) {
       if (p1!=null) {
           if (p2!=null) {
               return p1.getFirstPlayedCardValue() - p2.getFirstPlayedCardValue();
           } else {
               return -1; // p1 > null p2
           }
       } else {
           if (p2!=null) {
               return 1; // p2 > null p1
           } else {
               return 0; // null == null
           }
       }
    }
}

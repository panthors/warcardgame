package org.patrickjoiner.games.war;

/**
 * Card represents a playing card. It contains enum classes for the Suit and Rank of the card.
 */
public class Card {
    /**
     * Suit of the card; enum has all possible values.
     */
    public enum Suit {
        CLUB(1,"clubs","\u2663"),
        HEART(2,"hearts","\u2665"),
        DIAMOND(3,"diamonds","\u2666"),
        SPADE(4,"spades","\u2660");
        private final int value;
        public final String name;
        public final String symbol;

        Suit(int value, String name, String symbol) {
            this.value = value;
            this.name = name;
            this.symbol = symbol;
        }
    }

    /**
     * Rank of the card; enum has all possible values.
     */
    public enum Rank {
        TWO(2,"2"), THREE(3, "3"), FOUR(4,"4"), FIVE(5,"5"), SIX(6,"6"), SEVEN(7,"7"), EIGHT(8,"8"), NINE(9,"9"), TEN(10,"10"), JACK(11,"jack"), QUEEN(12,"queen"), KING(13,"king"), ACE(14,"ace");
        public final int value;
        public final String name;
        Rank(int value,String name) {
            this.value = value;
            this.name = name;
        }

    }

    private Suit suit;
    private Rank rank;

    /**
     * Default constructor; if suit/rank not specified,
     *    the card will always be the ace of spades.
     */
    public Card() {
        this.suit = Suit.SPADE;
        this.rank = Rank.ACE;
    }

    /**
     * Constructor with suit and rank.
     *    If suit is null, defaults to ace.
     *    If rank is null, defaults to spades.
     *
     *    @param cardSuit the Suit for the card
     *    @param cardRank the Rank for the card
     *
     */
    public Card(Suit cardSuit, Rank cardRank) {
        if (cardSuit == null) {
            this.suit = Suit.SPADE;
        } else {
            this.suit = cardSuit;
        }

        if (cardRank == null) {
            this.rank = Rank.ACE;
        } else {
            this.rank = cardRank;
        }
    }

    /**
     * Get the suit for the card
     * @return the Suit for the card
     */
    public Suit getSuit() {
        return suit;
    }

    /**
     * Get the rank of the card
     * @return the Rank of the card
     */
    public Rank getRank() {
        return rank;
    }

    /**
     * Returns a String representing an ASCII "image" of the card.
     *    The ASCII text is best viewed with a monospaced font
     *    but is readable with proportional fonts.
     *
     * @param showCard  boolean indicating if face of card should be shown
     * @return a String representing an ASCII image of the card
     */
    public String getAsciiImage(boolean showCard) {
        if (showCard) {
            String rankString = rank.value > 10 ? String.valueOf(rank.name.substring(0, 1).toUpperCase()) : String.valueOf(rank.value);
            String spaceString = String.format("%" + (5 - rankString.length()) + "s", "");
            return String.format(" ----- %n|%s%s|%n|  %s  |%n|%s%s|%n ----- ", rankString, spaceString, suit.symbol, spaceString, rankString);
        } else {
            return " ----- \n|*****|\n|*****|\n|*****|\n ----- ";
        }
    }

    /**
     * Returns a String representing the name of the card
     *
     * @return a String representing the name of the card
     */
    @Override
    public String toString() {
        return rank.name + " of " + suit.name;
    }
}

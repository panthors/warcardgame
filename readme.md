The card game of "War".

# Rules

## Normal round
A round consists of each player playing a card. The highest face value card wins, regardless of suit.

If more than one player has the highest face value card, those players will enter a war round.

## War round
In a war round, each player must play 2 cards face down. Each player reveals the first card, and the winner is the player with the highest face value card. 

In the event of a tie, the second card is revealed and the winner is the player with the highest face value card. 

The war round will be repeated until either there is a winner or no players have enough cards for another war round.

## Additional rules
- A player must have 2 cards to enter a war round; otherwise they lose the round.
- A player must win all 52 cards to win the game. If at the end of the game no player has 52 cards, then there is no winner.

# Running the game
The game requires Java 1.8 or greater to run.

The game is contained in an executable jar file, so to run use this command:

```
java -jar war-1.0-SNAPSHOT.jar -p [number of players]
```

The number of players must be provided, and must be a factor of 52 for each player to get the same number of cards.

# Building
The project was built using Maven; to build use this command:

```
mvn clean package
```